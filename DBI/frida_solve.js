// convenience function because ASLR addresses are difficult to read
function getAddress(modules, start) {
    for (let i = 0; i < modules.length; i += 1) {
        const base = modules[i].base;
        const size = modules[i].size;
        const limit = base.add(size);

        if (start.compare(base) < 0 || start.compare(limit) > 0) {
            continue;
        }

        const offset = start.sub(base).toInt32();
        return modules[i].name + '!' + offset.toString(16);
    }
    return start.toString();
}

function startStalking() {
    const modules = Process.enumerateModules();
    Stalker.follow(Process.getCurrentThreadId(),
        {
            events: { exec: true },

            onReceive(events) {
                events = Stalker.parse(events, {
                    annotate: true,
                    stringify: false
                }).map((event) => getAddress(modules, event[1]));
                send(events);
            }
        });
}

// Two things are needed for this to be successful:
// 1. Start stalking once the actual Thread runs, we do this by hooking into 
//    __libc_start_main.
// 2. Make sure everything is flushed before the application exists.
//    We do this by hooking exit() and flushing everything from there.

Interceptor.attach(Module.getExportByName(null, "exit"), {
    onEnter(args) {
        Stalker.flush();
        send("exit()");
        Thread.sleep(.1);
    }
});

const startMainAddress = Module.getExportByName(null, "__libc_start_main");
const startMainFunctionPointer = new NativeFunction(
    startMainAddress,
    "int",
    ["pointer", "int", "pointer", "pointer", "pointer", "pointer", "pointer"],
    { traps: "all" });

const startMainReplacement = new NativeCallback(
    (mainPtr, argc, argv, p1, p2, p3, p4) => {
        startStalking();
        return startMainFunctionPointer(mainPtr, argc, argv, p1, p2, p3, p4);
    },
    "int",
    ["pointer", "int", "pointer", "pointer", "pointer", "pointer", "pointer"]
);

Interceptor.replace(startMainAddress, startMainReplacement);
