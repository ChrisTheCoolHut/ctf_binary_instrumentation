for i in $(seq 1 30); do
	echo "input length : "$i;
	python2 -c "print 'A'*$i" | qemu-x86_64 -d in_asm,nochain ./l3 2>&1 | wc -l;
done
