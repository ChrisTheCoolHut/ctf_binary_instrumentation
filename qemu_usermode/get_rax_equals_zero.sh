#!/bin/bash
set -x
# Flag is 29 characters long (so 'A'*28 will have the highest number adding in the '\n')
python2 -c "print 'A'*27" | qemu-x86_64 -d cpu ./wyvern_c85f1be480808a9da350faaa6104a19b 2>&1 | grep 'RAX=0000000000000000' | wc -l
python2 -c "print 'A'*28" | qemu-x86_64 -d cpu ./wyvern_c85f1be480808a9da350faaa6104a19b 2>&1 | grep 'RAX=0000000000000000' | wc -l
python2 -c "print 'A'*29" | qemu-x86_64 -d cpu ./wyvern_c85f1be480808a9da350faaa6104a19b 2>&1 | grep 'RAX=0000000000000000' | wc -l

# You can see the same thing here:
python2 -c "print 'A'*27" | qemu-x86_64 -d in_asm,nochain ./wyvern_c85f1be480808a9da350faaa6104a19b 2>&1 | wc -l
python2 -c "print 'A'*28" | qemu-x86_64 -d in_asm,nochain ./wyvern_c85f1be480808a9da350faaa6104a19b 2>&1 | wc -l
python2 -c "print 'A'*29" | qemu-x86_64 -d in_asm,nochain ./wyvern_c85f1be480808a9da350faaa6104a19b 2>&1 | wc -l