#define _GNU_SOURCE
#include <stdio.h>
#include <dlfcn.h>

int iter = 0;

int strncmp(const char *s1, const char *s2, unsigned int n)
{
    if (s1[0] == 'f') return 1; // fish
    if (s1[0] == 'b') return 1; // bash
    if (s1[0] == 't') return 0; // tidbits
}

char *fgets(char *s, int size, FILE *stream)
{
    // Get original fgets
    typeof(&fgets) orig = dlsym(RTLD_NEXT, "fgets");
    iter++;
    if (iter < 2) return orig(s,size,stream);
    if (iter < 7) return 1; // Program only checks for not null
    return orig(s,size,stream);
}